function createCard(name, location, description, pictureUrl, date, date2) {
    return `
    <div class="col">
    <div class="card mb-3 shadow">
    <img src="${pictureUrl}" class="card-img-top">
    <div class="card-body">
        <h5 class="card-title">${name}</h5>
        <h6 class="subtitle">${location}</h6>
        <p class="card-text">${description}</p>
        <footer class="card-text"><small>Dates: ${date}-${date2}</small></footer>
    </div>
    </div>
    </div>
  `;
}


window.addEventListener('DOMContentLoaded', async () => {

    const url = 'http://localhost:8000/api/conferences/';

    try {
        const response = await fetch(url);

        if (!response.ok) {
            const error = await response.text();
            alert(`Something went wrong: ${error}`);
        } else {
        const data = await response.json();

        for (let conference of data.conferences) {
            const detailUrl = `http://localhost:8000${conference.href}`;
            const detailResponse = await fetch(detailUrl);
            if (detailResponse.ok) {
                const details = await detailResponse.json();
                const name = details.conference.name;
                const location = details.conference.location.name;
                const description = details.conference.description;
                const pictureUrl = details.conference.location.picture_url;
                const date = new Date(details.conference.starts).toLocaleDateString();
                const date2 = new Date(details.conference.ends).toLocaleDateString();
                const html = createCard(name, location, description, pictureUrl, date, date2);
                const row = document.querySelector('.row');
                row.innerHTML += html;
                console.log(details);
              }
            }

          }
        } catch (e) {
        console.error(e);
      }

    });
